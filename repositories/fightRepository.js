const { BaseRepository } = require("./baseRepository");

class FightRepository extends BaseRepository {
  constructor() {
    super("fights");
  }

  createFight(fighter1, fighter2) {
    const data = {
      id: this.generateId(),
      fighter1,
      fighter2,
      createdAt: new Date(),
      logs: [],
    };

    const list = this.dbContext.push(data).write();
    return list.find((it) => it.id === data.id);
  }
}

exports.FightRepository = new FightRepository();
