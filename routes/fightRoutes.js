const { Router } = require("express");
const FightService = require("../services/fightService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFightValid,
} = require("../middlewares/fight.validation.middleware.js");

const router = Router();

// OPTIONAL TODO: Implement route controller for fights

// GET /api/fight/:id
router.get(
  "/:id",
  (req, res, next) => {
    try {
      res.data = FightService.search(req.params.id);
      console.log(res.data);
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

// POST /api/fight
router.post(
  "/",
  createFightValid,
  (req, res, next) => {
    try {
      const { fighter1, fighter2 } = req.body;
      res.data = FightService.createFight(fighter1, fighter2);
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

// PUT /api/fight/:id
router.put(
  "/:id",

  (req, res, next) => {
    try {
      const { data } = req.body;
      res.data = FightService.addLogs(req.params.id, data);
    } catch (error) {
      res.error = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
