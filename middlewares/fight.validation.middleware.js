const createFightValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const { fighter1, fighter2, id } = req.body;

  if (!fighter1 || !fighter2) {
    const error = new Error("Please, provide fighters IDs");
    error.status = 400;
    res.error = error;
  }

  if (id) {
    const error = new Error("Do not like provided fight ID");
    error.status = 400;
    res.error = error;
  }

  next();
};

module.exports = { createFightValid };
