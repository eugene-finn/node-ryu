const { FightRepository } = require("../repositories/fightRepository");
const { FighterRepository } = require("../repositories/fighterRepository");

class FightService {
  // OPTIONAL TODO: Implement methods to work with fights

  search(fightId) {
    const fight = FightRepository.getOne({ id: fightId });
    console.log(fight);

    if (!fight) {
      const error = new Error("Fight is no found");
      error.status = 400;
      throw error;
    }
    return fight;
  }

  checkFighters(fighter1, fighter2) {
    const firstFighter = FighterRepository.getOne({ id: fighter1 });
    const secondFighter = FighterRepository.getOne({ id: fighter2 });
    if (!firstFighter || !secondFighter) {
      const error = new Error("There is no such register fighters");
      error.status = 400;
      throw error;
    }
  }

  checkFight(fight) {
    if (!fight) {
      const error = new Error("No such fight");
      error.status = 400;
      throw error;
    }
  }

  createFight(fighter1, fighter2) {
    this.checkFighters(fighter1, fighter2);

    return FightRepository.createFight(fighter1, fighter2);
  }

  addLogs(fightId, data) {
    const fight = FightRepository.getOne({ id: fightId });
    this.checkFight(fight);
    const { logs } = fight;
    const dataToUpdate = { logs: [...logs, ...data] };
    console.log(dataToUpdate);
    const updatedFight = FightRepository.update(fightId, dataToUpdate);

    return updatedFight;
  }
}

module.exports = new FightService();
